#include "app.h"

char *recv_msg(int conn_sock)
{
    int ret, nLeft, index = 0;
    char recv_data[BUFF_SIZE], *data;
    int msg_len;

    // receive the length of message
    int bytes_received = recv(conn_sock, &msg_len, sizeof(int), MSG_WAITALL);
    if (bytes_received <= 0)
    {
        return NULL;
    }

    if (msg_len > 0)
    {
        nLeft = msg_len;
        data = (char *)malloc(msg_len * sizeof(char));
        memset(data, 0, msg_len);
        index = 0;

        //receives message from client
        while (nLeft > 0)
        {
            ret = recv(conn_sock, recv_data, BUFF_SIZE, 0);
            if (ret == -1)
            {
                return NULL;
            }
            memcpy(data + index, recv_data, ret);
            index += ret;
            nLeft -= ret;
        }
        data[msg_len] = '\0';
    }
    return data;
}

int send_msg(int conn_sock, char *message)
{
    int msg_len, bytes_sent;
    //send the length of the message to client
    msg_len = strlen(message);
    bytes_sent = send(conn_sock, &msg_len, sizeof(int), 0);
    if (bytes_sent <= 0)
    {
        return -1;
    }

    // send message to client
    bytes_sent = send(conn_sock, message, msg_len, 0);
    if (bytes_sent <= 0)
    {
        return -1;
    }
    return 0;
}

// load user data from account file
int load_users_data(struct User *users)
{
    FILE *fp = NULL;
    char line[100];
    char *user_id, *password;
    int status = NOT_FOUND, index = 0;
    if ((fp = fopen("account.txt", "r")) == NULL)
    {
        printf("Cannot open account file\n");
        return -1;
    }
    while (fgets(line, 100, fp))
    {
        // read user information from file
        user_id = strtok(line, " ");
        if (user_id == NULL)
            break;
        password = strtok(NULL, " ");
        if (password == NULL)
            break;
        status = atoi(strtok(NULL, " "));
        if (status == 0)
            break;
        // store them in users[] array
        strcpy(users[index].id, user_id);
        strcpy(users[index].pass, password);
        users[index].accStatus = status;
        //printf("USER ID: %s, PASS: %s, STATUS: %d\n", users[index].id, users[index].pass, users[index].accStatus );
        index++;
        memset(line, 0, 100);
    }
    return index;
}

// initialize sessions
void init_sessions(struct Session *sessions)
{
    int i = 0;
    for (i = 0; i < 100; i++)
    {
        sessions[i].sessStatus = NOT_CONNECTED;
        sessions[i].conn_sock = -1;
        sessions[i].number_of_retries = 0;
        sessions[i].room_number = -1;
    }
}

// return index of connected socket or create a new session if socket not connected
int create_session(struct Session *sessions, int conn_sock)
{
    int i = 0;
    for (i = 0; i < 100; i++)
    {
        if (sessions[i].conn_sock == conn_sock)
        {
            printf("\nConnected socket\n");
            return i;
        }
    }

    for (i = 0; i < 100; i++)
    {
        if (sessions[i].sessStatus == NOT_CONNECTED)
        {
            strcpy(sessions[i].user.id, "");
            strcpy(sessions[i].user.pass, "");
            sessions[i].user.accStatus = NOT_FOUND;
            sessions[i].sessStatus = NOT_IDENTIFIED_USER;
            sessions[i].conn_sock = conn_sock;
            return i;
        }
    }
    return -1;
}

void handle_message(char *recv_msg, struct Session *sessions, int current_session, struct User *users, int *number_of_users, struct Room *list_rooms, char *reply_message)
{
    char *token1, *token2, *token3, *token4;
    int cmd = 0;
    int user_status;
    int room_number, i;
    token1 = strtok(recv_msg, " ");
    token2 = strtok(NULL, " ");
    token3 = strtok(NULL, " ");
    token4 = strtok(NULL, " ");
    cmd = detect_message(token1);
    int result = 0;
    switch (cmd)
    {
    case LOGIN:
        //printf("Handling user login\n");
        result = login_find_user_id(sessions, current_session, users, *number_of_users, token2, token3);
        switch (result)
        {
        case NOT_FOUND:
            strcpy(reply_message, "01");
            break;
        case SUCCESS:
            strcpy(reply_message, "00");
            break;
        case BLOCKED:
            strcpy(reply_message, "F1");
            break;
        case INVALID_COMMAND:
            strcpy(reply_message, "F2");
            break;
        default:
            break;
        }
        break;
    case LOUT:
        //printf("Handling user logout\n\n");
        result = logout_find_user_id(sessions, current_session, token2);
        switch (result)
        {
        case NOT_FOUND:
            strcpy(reply_message, "11");
            break;
        case SUCCESS:
            result = logout_check_password(sessions, current_session, users, *number_of_users, token3);
            switch (result)
            {
            case NOT_FOUND:
                strcpy(reply_message, "F0");
                break;
            case SUCCESS:
                strcpy(reply_message, "10");
                break;
            case INVALID_COMMAND:
                strcpy(reply_message, "F2");
                break;
            default:
                break;
            }
            break;
        case INVALID_COMMAND:
            strcpy(reply_message, "F2");
            break;
        default:
            break;
        }
        break;
    case SIGNUP:
        result = signup_find_user_id(sessions, current_session, users, *number_of_users, token2);
        switch (result)
        {
        case NOT_FOUND:
            result = signup_password(sessions, current_session, users, number_of_users, token2, token3);
            switch (result)
            {
            case NOT_FOUND:
                strcpy(reply_message, "22");
                break;
            case SUCCESS:
                strcpy(reply_message, "20");
                break;
            case INVALID_COMMAND:
                strcpy(reply_message, "F2");
                break;
            default:
                break;
            }
            break;
        case SUCCESS:
            strcpy(reply_message, "21");
            break;
        case INVALID_COMMAND:
            strcpy(reply_message, "F2");
            break;
        default:
            break;
        }
        break;
    case CRRM:
        result = create_room(sessions, current_session, token2, list_rooms);
        if (result == 0)
        {
            strcpy(reply_message, "R11");
        }
        else if (result == 1)
        {
            strcpy(reply_message, "R01");
        }
        else if (result == 2)
        {
            strcpy(reply_message, "R02");
        }
        else if (result == 3)
        {
            strcpy(reply_message, "R03");
        }
        break;
    case LIRM:
        list_available_rooms(sessions, current_session, list_rooms);
        break;
    case JOIN:
        result = join_room(sessions, current_session, token2, list_rooms);
        if (result == 0)
        {
            strcpy(reply_message, "R11");
        }
        else if (result == 1)
        {
            strcpy(reply_message, "R01");
        }
        else if (result == 2)
        {
            strcpy(reply_message, "R02");
        }
        else if (result == 3)
        {
            strcpy(reply_message, "R03");
        }
        break;
    case LEAVE:
        result = leave_room(sessions, current_session);
        printf("result %d\n", result);
        switch (result)
        {
        case 0:
            strcpy(reply_message, "L11");
            break;
        case 1:
            strcpy(reply_message, "L01");
            break;
        case 2:
            strcpy(reply_message, "L03");
            break;
        default:
            strcpy(reply_message, "F2");
            break;
        }
        break;
    case DELR:
        result = del_room(sessions, current_session, token2, list_rooms);
        switch (result)
        {
        case 0:
            strcpy(reply_message, "D11");
            break;
        case 1:
            strcpy(reply_message, "D01");
            break;
        case 2:
            strcpy(reply_message, "D02");
            break;
        case 3:
            strcpy(reply_message, "D03");
            break;
        default:
            strcpy(reply_message, "F2");
            break;
        }
        break;
    case CRPD:
        //token 1: product name
        //token 2: thresh price
        user_status = sessions[current_session].sessStatus;
        switch (user_status)
        {
        case NOT_CONNECTED:
        case NOT_IDENTIFIED_USER:
        case NOT_AUTHENTICATED:
            strcpy(reply_message, "CRPD03"); //have to log in
            break;
        case AUTHENTICATED:
            strcpy(reply_message, "CRPD01"); //have to join a room
            break;
        case IN_ROOM: //already in a room
            room_number = sessions[current_session].room_number;
            for (i = 0; i < 100; i++)
            {
                if (list_rooms[i].room_number == room_number)
                {
                    enQueue(&list_rooms[i], sessions[current_session].user.id, token2, token3, token4);
                    // Neu san pham o dau queue thi bat dau dau gia san pham, gui thong bao toi tat ca nguoi dung trong phong
                    strcpy(reply_message, "CRPD11");
                    inform_product(list_rooms[i], sessions);
                    break;
                }
            }
            break;
        }
        // enQueue(token1, token2, token3);
        break;
    case SBUY:
        result = instant_buy(sessions, current_session, list_rooms);
        switch (result)
        {
        case NOT_AUTHENTICATED:
            strcpy(reply_message, "SBUY10");
            break;
        case AUTHENTICATED:
            strcpy(reply_message, "SBUY11");
            break;
        case IN_ROOM:
            strcpy(reply_message, "SBUY00");
            break;
        case INVALID_COMMAND:
            strcpy(reply_message, "F2");
            break;
        default:
            break;
        }
        break;
    case AUCP:
        user_status = sessions[current_session].sessStatus;
        result = auction(sessions, current_session, list_rooms, token2);
        switch (user_status)
        {
        case NOT_CONNECTED:
        case NOT_IDENTIFIED_USER:
        case NOT_AUTHENTICATED:
            strcpy(reply_message, "AUCP10"); //have to log in
            break;
        case AUTHENTICATED:
            strcpy(reply_message, "AUCP11"); //have to join a room
            break;
        case IN_ROOM: //already in a room
            strcpy(reply_message, "AUCP00"); // auctioning
            break;
        }
        break;
    case EXIT:
        change_session_status_log_out(sessions, current_session);
        strcpy(reply_message, "EXIT");
        break;
    default:
        strcpy(reply_message, "F2");
        break;
    }
}

int detect_message(char *token)
{
    if (strcmp(token, "LOGIN") == 0)
    {
        return LOGIN;
    }
    else if (strcmp(token, "SIGNUP") == 0)
    {
        return SIGNUP;
    }
    else if (strcmp(token, "LOUT") == 0)
    {
        return LOUT;
    }
    else if (strcmp(token, "CRRM") == 0)
    {
        return CRRM;
    }
    else if (strcmp(token, "JOIN") == 0)
    {
        return JOIN;
    }
    else if (strcmp(token, "CRPD") == 0)
    {
        return CRPD;
    }
    else if (strcmp(token, "AUCP") == 0)
    {
        return AUCP;
    }
    else if (strcmp(token, "LEAVE") == 0)
    {
        return LEAVE;
    }
    else if (strcmp(token, "SBUY") == 0)
    {
        return SBUY;
    }
    else if (strcmp(token, "DELR") == 0)
    {
        return DELR;
    }
    else if (strcmp(token, "DELP") == 0)
    {
        return DELP;
    }
    else if (strcmp(token, "SBUY") == 0)
    {
        return SBUY;
    }
    else if (strcmp(token, "AUCP") == 0)
    {
        return AUCP;
    }
    else if (strcmp(token, "LIRM") == 0)
    {
        return LIRM;
    }
    else if (strcmp(token, "EXIT") == 0)
    {
        return EXIT;
    }
    return INVALID_COMMAND;
}

int login_find_user_id(Session *sessions, int current_session, User *users, int number_of_users, char *user_id, char *password)
{
    int i = 0;
    if ((sessions[current_session].sessStatus == NOT_IDENTIFIED_USER) && (user_id != NULL))
    {
        for (i = 0; i < number_of_users; i++)
        { // find user id
            if (strcmp(users[i].id, user_id) == 0)
            {
                if (check_user_status(users[i]) == ACTIVE)
                {                                                                              // user active
                    change_session_status_correct_userid(sessions, current_session, users[i]); //Not authenticated
                    return login_check_password(sessions, current_session, users, number_of_users, password);
                }
                else
                { // user blocked
                    return BLOCKED;
                }
            }
        }
        return NOT_FOUND; // user not found
    }
    else
    {
        printf("NOT IDENTIFIED USER\n");
        return INVALID_COMMAND;
    }
}

int check_user_status(User user)
{
    return user.accStatus;
}

void change_session_status_correct_userid(Session *sessions, int current_session, User user)
{
    if (strcmp((sessions[current_session].user).id, user.id) == 0)
    {
        return;
    }
    else
    {
        (sessions[current_session].user).accStatus = user.accStatus;
        strcpy((sessions[current_session].user).id, user.id);
        strcpy((sessions[current_session].user).pass, user.pass);
        sessions[current_session].number_of_retries = 0;
    }
}

int login_check_password(Session *sessions, int current_session, User *users, int number_of_users, char *password)
{
    if (password != NULL)
    {
        if (strcmp((sessions[current_session].user).pass, password) == 0)
        {
            change_session_status_correct_password(sessions, current_session);
            sessions[current_session].number_of_retries = 0;
            return SUCCESS;
        }
        else
        {
            sessions[current_session].number_of_retries += 1;
            if (sessions[current_session].number_of_retries == 3)
            {
                block_user_id(sessions, current_session, users, number_of_users);
                return BLOCKED;
            }
            else
                return NOT_FOUND;
        }
    }
    else
        return INVALID_COMMAND;
}

void change_session_status_correct_password(Session *sessions, int current_session)
{
    sessions[current_session].sessStatus = AUTHENTICATED;
}

void block_user_id(Session *sessions, int current_session, User *users, int number_of_users)
{
    int i = 0;
    sessions[current_session].user.accStatus = BLOCKED;
    sessions[current_session].sessStatus = NOT_IDENTIFIED_USER;
    for (i = 0; i < number_of_users; i++)
    {
        if (strcmp(users[i].id, sessions[current_session].user.id) == 0)
        {
            users[i].accStatus = BLOCKED;
        }
    }
}

int logout_find_user_id(Session *sessions, int current_session, char *user_id)
{
    if (sessions[current_session].sessStatus == AUTHENTICATED)
    {
        if (strcmp(sessions[current_session].user.id, user_id) == 0)
        {
            return SUCCESS;
        }
        else
        {
            return NOT_FOUND;
        }
    }
    else
        return INVALID_COMMAND;
}

void change_session_status_log_out(Session *sessions, int current_session)
{
    sessions[current_session].sessStatus = NOT_IDENTIFIED_USER;
    strcpy(sessions[current_session].user.id, "");
    strcpy(sessions[current_session].user.pass, "");
    sessions[current_session].user.accStatus = NOT_FOUND;
}

int logout_check_password(Session *sessions, int current_session, User *users, int number_of_users, char *password)
{
    if (sessions[current_session].sessStatus == AUTHENTICATED && (password != NULL))
    {
        if (strcmp((sessions[current_session].user).pass, password) == 0)
        {
            change_session_status_log_out(sessions, current_session);
            sessions[current_session].number_of_retries = 0;
            return SUCCESS;
        }
        else
        {
            return NOT_FOUND;
        }
    }
    else
        return INVALID_COMMAND;
}

int signup_find_user_id(Session *sessions, int current_session, User *users, int number_of_users, char *user_id)
{
    int i = 0;
    if ((sessions[current_session].sessStatus == NOT_IDENTIFIED_USER) && (user_id != NULL))
    {
        for (i = 0; i < number_of_users; i++)
        { // find user id
            if (strcmp(users[i].id, user_id) == 0)
            {
                return SUCCESS;
            }
        }
        return NOT_FOUND; // user not found
    }
    else
        return INVALID_COMMAND;
}

int signup_password(Session *sessions, int current_session, User *users, int *number_of_users, char *user_id, char *password)
{
    if (sessions[current_session].sessStatus == NOT_IDENTIFIED_USER && (password != NULL))
    {
        if (strlen(password) >= 8)
        {
            strcpy(users[*number_of_users].id, user_id);
            strcpy(users[*number_of_users].pass, password);
            users[*number_of_users].accStatus = ACTIVE;
            signup_correct_password(sessions, current_session, users[*number_of_users]);
            sessions[current_session].number_of_retries = 0;
            (*number_of_users)++;
            return SUCCESS;
        }
        else
        {
            return NOT_FOUND;
        }
    }
    else
        return INVALID_COMMAND;
}

void signup_correct_password(Session *sessions, int current_session, User user)
{
    sessions[current_session].sessStatus = AUTHENTICATED;
    (sessions[current_session].user).accStatus = user.accStatus;
    strcpy((sessions[current_session].user).id, user.id);
    strcpy((sessions[current_session].user).pass, user.pass);
}

int save_users_data(User *users, int number_of_users)
{
    FILE *fp = NULL;
    int i = 0;
    if ((fp = fopen("account.txt", "w")) == NULL)
    {
        printf("Cannot open account file\n");
        return -1;
    }
    printf("Number of users: %d\n", number_of_users);
    for (i = 0; i < number_of_users; i++)
    {
        fprintf(fp, "%s ", users[i].id);
        printf("%s ", users[i].id);
        fprintf(fp, "%s ", users[i].pass);
        printf("%s ", users[i].pass);
        fprintf(fp, "%d\n", users[i].accStatus);
        printf("%d\n", users[i].accStatus);
    }
    fclose(fp);
    return 0;
}

int generate_list_rooms(struct Room *list_rooms, int list_rooms_length)
{
    int i = 0;
    for (i = 0; i < list_rooms_length; i++)
    {
        list_rooms[i].room_number = -1;
        list_rooms[i].front = NULL;
        list_rooms[i].rear = NULL;
    }
    return 1;
}

int create_room(Session *sessions, int current_session, char *room_number, struct Room *list_rooms)
{
    int i = 0;
    int can_created_room = 0;
    switch (sessions[current_session].sessStatus)
    {
    case NOT_CONNECTED:
    case NOT_IDENTIFIED_USER:
    case NOT_AUTHENTICATED:
        return 3; // not log in
        break;
    case IN_ROOM:
        return 2; // already in a room
        break;
    }
    for (i = 0; i < 100; i++)
    {
        if (list_rooms[i].room_number == -1)
        {
            list_rooms[i].owner = sessions[current_session].user;
            list_rooms[i].front = NULL;
            list_rooms[i].rear = NULL;
            list_rooms[i].room_number = atoi(room_number);
            can_created_room = 1;
            break;
        }
    }
    if (can_created_room)
    {
        printf("User %s created and join room %d\n", list_rooms[i].owner.id, list_rooms[i].room_number);
        sessions[current_session].room_number = atoi(room_number);
        sessions[current_session].sessStatus = IN_ROOM;
        return 0;
    }
    else
    {
        printf("Can not created room\n");
        return 1;
    }
}

int join_room(Session *sessions, int current_session, char *room_number, struct Room *list_rooms)
{
    int i = 0;
    int can_join_room = 0;
    int room_number_i = atoi(room_number);
    switch (sessions[current_session].sessStatus)
    {
    case NOT_CONNECTED:
    case NOT_IDENTIFIED_USER:
    case NOT_AUTHENTICATED:
        return 3; // not log in
        break;
    case IN_ROOM:
        return 2; // already in a room
        break;
    }
    for (i = 0; i < 100; i++)
    {
        if (list_rooms[i].room_number == room_number_i)
        {
            sessions[current_session].room_number = atoi(room_number);
            sessions[current_session].sessStatus = IN_ROOM;
            can_join_room = 1;
            printf("User %s joined room %d\n", list_rooms[i].owner.id, list_rooms[i].room_number);
        }
    }
    if (can_join_room)
    {
        return 0;
    }
    else
    {
        return 1; //don't exist room number
    }
}

int leave_room(Session *sessions, int current_session)
{
    switch (sessions[current_session].sessStatus)
    {
    case NOT_CONNECTED:
    case NOT_IDENTIFIED_USER:
    case NOT_AUTHENTICATED:
        return 3; // not log in
        break;
    case AUTHENTICATED:
        return 1; // not be in a room
        break;
    default: //in a room
        //TODO: check khach hang dang tra gia cao nhat
        sessions[current_session].sessStatus = AUTHENTICATED;
        sessions[current_session].room_number = -1;
        return 0;
    }
}

int del_room(Session *sessions, int current_session, char *room_number, struct Room *list_rooms)
{
    int i = 0;
    if (sessions[current_session].sessStatus == AUTHENTICATED)
        return 1; // not in a room
    if (sessions[current_session].sessStatus != IN_ROOM)
        return 3; //have to log in
    for (i = 0; i < 100; i++)
    {
        if (strcmp(list_rooms[i].owner.id, sessions[current_session].user.id) == 0)
        {
            leave_room(sessions, current_session);
            list_rooms[i].room_number = -1;
            if (list_rooms[i].front)
                free(list_rooms[i].front);
            if (list_rooms[i].rear)
                free(list_rooms[i].rear);
            list_rooms[i].front = NULL;
            list_rooms[i].rear = NULL;
            return 0;
        }
    }
    return 2; //don't have permission
}

struct ProductNode *newNode(char *seller_id, char *productName, char *product_info, int thresh_price)
{
    struct ProductNode *temp = (struct ProductNode *)malloc(sizeof(struct ProductNode));
    struct Product *product = (struct Product *)malloc(sizeof(struct Product));
    strcpy(product->seller_id, seller_id);
    strcpy(product->productName, productName);
    strcpy(product->productDescription, product_info);
    product->thresh_price = thresh_price;
    product->currentHighestPrice = 0;
    product->productStatus = NOT_AUCTION;

    temp->product = *product;
    temp->next_Product = NULL;
    return temp;
}

void enQueue(struct Room *room, char *seller_id, char *productName, char *product_info, char *thresh_price)
{
    // Create a new LL node
    struct ProductNode *temp = newNode(seller_id, productName, product_info, atoi(thresh_price));

    // If queue is empty, then new node is front and rear both
    if (room->rear == NULL)
    {
        room->front = room->rear = temp;
        printf("Da nhap vao mat hang moi\nname: %s\nnguoi ban: %s\n gia ban ngay: %s\n\n", productName, seller_id, thresh_price);
        return;
    }

    // Add the new node at the end of queue and change rear
    room->rear->next_Product = temp;
    room->rear = temp;

    //check
    printf("Da nhap vao mat hang moi\nname: %s\nnguoi ban: %s\n gia ban ngay: %s\n\n", productName, seller_id, thresh_price);
}

struct ProductNode *deQueue(struct Room *room)
{
    // If queue is empty, return NULL.
    if (room->front == NULL)
        return NULL;

    // Store previous front and move front one node ahead
    struct ProductNode *temp = room->front;
    room->front = room->front->next_Product;

    // If front becomes NULL, then change rear also as NULL
    if (room->front == NULL)
        room->rear = NULL;
    return temp;
}

int instant_buy(Session *sessions, int current_session, struct Room *list_rooms)
{
    int user_status = sessions[current_session].sessStatus;
    switch (user_status)
    {
    case NOT_CONNECTED:
    case NOT_IDENTIFIED_USER:
    case NOT_AUTHENTICATED:
        return NOT_AUTHENTICATED;
    case AUTHENTICATED:
        return AUTHENTICATED;
    case IN_ROOM: //already in a room
    {
        int room_number = sessions[current_session].room_number;
        int i;
        for (i = 0; i < 100; i++)
        {
            if (list_rooms[i].room_number == room_number)
            {
                struct ProductNode *frontNode = list_rooms[i].front;
                if (frontNode != NULL)
                {
                    frontNode->product.productStatus = SOLD;
                    strcpy(frontNode->product.currentBidder_id, sessions[current_session].user.id);
                    frontNode->product.currentHighestPrice = frontNode->product.thresh_price;
                    inform_product(list_rooms[i], sessions);
                    deQueue(&list_rooms[i]);
                    save_auction_data(frontNode->product);
                    inform_product(list_rooms[i], sessions);
                    return IN_ROOM;
                }
                break;
            }
        }
    }
    default:
        return INVALID_COMMAND;
    }
}

int inform_product(struct Room room, Session *sessions)
{
    struct ProductNode *frontNode = room.front;
    char inform_message[400];
    if (frontNode != NULL)
    {
        if (frontNode->product.productStatus == NOT_AUCTION)
        {
            sprintf(inform_message, "Start Auctioning Product: %s\nProduct Description:%s\nSeller: %s\nInstant buy price: %d\n\n",
                    frontNode->product.productName, frontNode->product.productDescription, frontNode->product.seller_id, frontNode->product.thresh_price);
            frontNode->product.productStatus = IN_AUCTION;
        }
        else if (frontNode->product.productStatus == IN_AUCTION)
        {
            sprintf(inform_message, "Auctioning Product: %s\nProduct Description:%s\nSeller: %s\nInstant Buy Price: %d\nCurrent Highest Price: %d\n\n", frontNode->product.productName, frontNode->product.productDescription, frontNode->product.seller_id, frontNode->product.thresh_price, frontNode->product.currentHighestPrice);
        }
        else if (frontNode->product.productStatus == SOLD)
        {
            sprintf(inform_message, "Product Sold: %s\nProduct Description:%s\nSeller: %s\nBought Price: %d\n\n", frontNode->product.productName, frontNode->product.productDescription, frontNode->product.seller_id, frontNode->product.currentHighestPrice);
        }
        else
            return -1;
        int i;
        for (i = 0; i < 100; i++)
        {
            if (sessions[i].room_number == room.room_number)
            {
                send_msg(sessions[i].conn_sock, inform_message);
            }
        }
        return frontNode->product.productStatus;
    }
    else
    {
        sprintf(inform_message, "Waiting for product to be enqueued...\n\n");
        int i;
        for (i = 0; i < 100; i++)
        {
            if (sessions[i].room_number == room.room_number)
            {
                send_msg(sessions[i].conn_sock, inform_message);
            }
        }
        return -1;
    }
}

int save_auction_data(struct Product product)
{
    FILE *fp = NULL;
    if ((fp = fopen("auction_log.txt", "a+")) == NULL)
    {
        printf("Cannot open account file\n");
        return -1;
    }
    fprintf(fp, "%s\n%s\n%s\n%s\n%d\n\n", product.productName, product.productDescription, product.seller_id, product.currentBidder_id, product.currentHighestPrice);
    fclose(fp);
    return 0;
}

int auction(Session *sessions, int current_session, struct Room *list_rooms, char *auction_price)
{
    int user_status = sessions[current_session].sessStatus;
    switch (user_status)
    {
    case NOT_CONNECTED:
    case NOT_IDENTIFIED_USER:
    case NOT_AUTHENTICATED:
        return NOT_AUTHENTICATED;
    case AUTHENTICATED:
        return AUTHENTICATED;
    case IN_ROOM: //already in a room
    {
        int room_number = sessions[current_session].room_number;
        int i;
        for (i = 0; i < 100; i++)
        {
            if (list_rooms[i].room_number == room_number)
            {
                struct ProductNode *frontNode = list_rooms[i].front;
                if (frontNode != NULL)
                {
                    int price = atoi(auction_price);
                    if (price >= frontNode->product.currentHighestPrice + 10000)
                    {
                        struct Thread_Arguments *thread_args = (struct Thread_Arguments *)malloc(sizeof(struct Thread_Arguments));
                        pthread_t tid;
                        frontNode->product.currentHighestPrice = price;
                        strcpy(frontNode->product.currentBidder_id, sessions[current_session].user.id);
                        *thread_args = (struct Thread_Arguments){.sessions = sessions, .room = list_rooms[i]};
                        pthread_create(&tid, NULL, &thread_process, (void *)thread_args);
                        //inform_product(list_rooms[i], sessions);
                    }
                    else
                    {
                        char message[1024] = "Auction Price must be 10.000 VND higher than current highest price\n";
                        send_msg(sessions[current_session].conn_sock, message);
                    }
                    return IN_ROOM;
                }
                break;
            }
        }
    }
    default:
        return INVALID_COMMAND;
    }
}

void list_available_rooms(Session *sessions, int current_session, struct Room *list_rooms)
{
    int i;
    char message[1024] = "\nList of available rooms:";
    for (i = 0; i < 100; i++)
    {
        if (list_rooms[i].room_number != -1)
        {
            sprintf(message, "%s %d", message, list_rooms[i].room_number);
        }
    }
    send_msg(sessions[current_session].conn_sock, message);
}

void *thread_process(void *args)
{
    struct Thread_Arguments *thread_args = (struct Thread_Arguments *)args;
    int previous_price = thread_args->room.front->product.currentHighestPrice;
    int times = 0;
    int retval = 101;
    inform_product(thread_args->room, thread_args->sessions);
    sleep(30);
    while (previous_price == thread_args->room.front->product.currentHighestPrice && times <= 3)
    {
        inform_product(thread_args->room, thread_args->sessions);
        times++;
        sleep(10);
    }
    if (times > 3)
    {
        struct ProductNode *frontNode = thread_args->room.front;
        if (frontNode != NULL)
        {
            frontNode->product.productStatus = SOLD;
            inform_product(thread_args->room, thread_args->sessions);
            deQueue(&(thread_args->room));
            save_auction_data(frontNode->product);
            inform_product(thread_args->room, thread_args->sessions);
        }
    }
    pthread_exit(&retval);
}