//Example code: A simple server side code, which echos back the received message.
//Handle multiple socket connections with select and fd_set on Linux
#include "app.h"
#include <stdio.h>
#include <string.h> //strlen
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>    //close
#include <arpa/inet.h> //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros

#define TRUE 1
#define FALSE 0

int main(int argc, char *argv[])
{
    int opt = TRUE;
    int master_socket, addrlen, new_socket, client_socket[100],
        max_clients = 100, activity, i, sd, current_session;
    int serv_port;
    int max_sd;
    struct sockaddr_in address;
    struct Room list_rooms[100];
    int number_of_users = 0;
    struct Session sessions[100];
    struct User users[100];
    char *data;
    char reply_message[30];
    char *endptr;

    //set of socket descriptors
    fd_set readfds;
    if ((number_of_users = load_users_data(users)) == -1)
    {
        return -1;
    }
    init_sessions(sessions);
    generate_list_rooms(list_rooms, 100);

    // Check terminal command arguments
    if (argc != 2)
    {
        printf("Invalid arguments\n");
        exit(-1);
    }
    serv_port = (in_port_t)strtol(argv[1], &endptr, 10);
    if (strlen(endptr) != 0)
    {
        printf("Invalid port!\n");
        exit(-1);
    }

    //initialise all client_socket[] to 0 so not checked
    for (i = 0; i < max_clients; i++)
    {
        client_socket[i] = -1;
    }

    //create a master socket
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //set master socket to allow multiple connections ,
    //this is just a good habit, it will work without this
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
                   sizeof(opt)) < 0)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //type of socket created
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(serv_port);

    //bind the socket to localhost port
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", serv_port);

    //try to specify maximum of 3 pending connections for the master socket
    if (listen(master_socket, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //accept the incoming connection
    addrlen = sizeof(address);
    puts("Waiting for connections ...");

    while (TRUE)
    {
        //clear the socket set
        FD_ZERO(&readfds);
        //add master socket to set
        FD_SET(master_socket, &readfds);
        max_sd = master_socket;
        //add child sockets to set
        for (i = 0; i < max_clients; i++)
        {
            //socket descriptor
            sd = client_socket[i];

            //if valid socket descriptor then add to read list
            if (sd > 0)
                FD_SET(sd, &readfds);

            //highest file descriptor number, need it for the select function
            if (sd > max_sd)
                max_sd = sd;
        }

        //wait for an activity on one of the sockets , timeout is NULL ,
        //so wait indefinitely
        activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);

        if ((activity < 0) && (errno != EINTR))
        {
            printf("select error");
            break;
        }

        //If something happened on the master socket ,
        //then its an incoming connection
        if (FD_ISSET(master_socket, &readfds))
        {
            if ((new_socket = accept(master_socket,
                                     (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }

            //inform user of socket number - used in send and receive commands
            printf("New connection , socket fd is %d , ip is : %s , port : %d\n", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

            //add new socket to array of sockets
            for (i = 0; i < max_clients; i++)
            {
                //if position is empty
                if (client_socket[i] < 0)
                {
                    client_socket[i] = new_socket;
                    current_session = create_session(sessions, new_socket);
                    // create new session or connect to previously established one
                    printf("Adding to list of sockets as %d of session %d\n", i, current_session);

                    break;
                }
            }
        }

        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++)
        {
            sd = client_socket[i];
            if (sd < 0)
                continue;
            current_session = create_session(sessions, sd);
            if (FD_ISSET(sd, &readfds))
            {
                printf("Socket: %d at session %d\n", sd, current_session);
                //Check if it was for closing , and also read the
                //incoming message
                if ((data = recv_msg(sd)) == NULL)
                {
                    //Somebody disconnected , get his details and print
                    getpeername(sd, (struct sockaddr *)&address,
                                (socklen_t *)&addrlen);
                    printf("Host disconnected , ip %s , port %d \n",
                           inet_ntoa(address.sin_addr), ntohs(address.sin_port));

                    //Close the socket and mark as 0 in list for reuse
                    close(sd);
                    client_socket[i] = -1;
                }

                //Echo back the message that came in
                else
                {
                    printf("%s\n", data);
                    handle_message(data, sessions, current_session, users, &number_of_users, list_rooms, reply_message);
                    if (send_msg(sd, reply_message) == -1)
                        break;
                    if (save_users_data(users, number_of_users) == -1)
                        break;
                }
            }
        }
    }

    return 0;
}